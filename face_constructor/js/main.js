(function () {

    function preloadImages (imgs, callback) {
        var img, count = imgs.length, i;
        for (i=0; i < imgs.length; i++) {
            img = new Image();
            img.onload = (function (i, img) {
                return function () {
                    imgs[i] = img;
                    count--;
                    if (!count) callback(imgs);
                };
            })(i, img)
            img.src = imgs[i];
        }
    }

    var stage = new Kinetic.Stage({
        container: 'container',
        width    : 128,
        height   : 128
    });

    var layerNames = ['head','eyes','mouth','nose','beard','hair'];
    var layers     = {};
    var positions  = [0, 0, 0, 0, 0, 0];
    var images     = [];
    layerNames.forEach(function (name) {
        layers[name] = new Kinetic.Layer();
        images.push('i/flora-'+name+'.png');
    });

    preloadImages(images, function (imageObjects) {
        for (var i=0; i < layerNames.length; i++) {
            var asset = new Kinetic.Image({
                x: 0,
                y: 0,
                image: imageObjects[i],
                width: 128,
                height: 128,
                crop: {
                    x: 0,
                    y: 0,
                    width: 128,
                    height: 128
                }
            });
            layers[layerNames[i]].add(asset);

            document.getElementById(layerNames[i]).onclick = (function (i, img, asset) {
                return function () {
                    positions[i] += 128
                    positions[i] %= img.height;
                    asset.setCrop({
                        x: 0,
                        y: positions[i],
                        width: 128,
                        height: 128
                    });
                    layers[layerNames[i]].draw();
                };
            })(i, imageObjects[i], asset);
        }

        layerNames.forEach(function (name) {
            stage.add(layers[name]);
        });
    });
})();
